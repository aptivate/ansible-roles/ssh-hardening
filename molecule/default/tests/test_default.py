def test_ssh_installed(host):
    ssh_package = host.package('openssh')

    assert ssh_package.is_installed


def test_ssh_is_running(host):
    ssh_service = host.service('sshd')

    assert ssh_service.is_running
    assert ssh_service.is_enabled


def test_ssh_stops_password_authentication(host):
    ssh_config = host.file('/etc/ssh/sshd_config')
    with host.sudo():
        assert ssh_config.contains('PasswordAuthentication no')


def test_ssh_stops_root_access(host):
    ssh_config = host.file('/etc/ssh/sshd_config')
    with host.sudo():
        assert ssh_config.contains('PermitRootLogin no')


def test_ssh_exposes_default_high_port(host):
    ssh_config = host.file('/etc/ssh/sshd_config')
    with host.sudo():
        assert ssh_config.contains('Port 48001')
