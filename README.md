[![pipeline status](https://git.coop/aptivate/ansible-roles/ssh-hardening/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/ssh-hardening/commits/master)

# ssh-hardening

A role to harden SSH access.

# Requirements

None.

# Role Variables

  * `disable_root_ssh`: Stop root SSH access
    * Defaults to `true`

  * `disable_password_auth`: Stop password authentication
    * Defaults to `true`

  * `ssh_default_port`: Which port to expose SSH access on.
    * Defaults to `48001`.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: ssh-hardening
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```bash
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
